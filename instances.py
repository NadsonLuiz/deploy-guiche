AWS_KEY = 'AKIAJ36QCNANPZ54AISQ'
AWS_SECRET = '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'

import boto.ec2

def get_connection():
    return boto.ec2.connect_to_region("sa-east-1",aws_access_key_id=AWS_KEY,aws_secret_access_key=AWS_SECRET)


def get_all_instances(filter_p):
    conn = get_connection()
    return conn.get_all_instances(filters=filter_p)

def stop_test_instances():
    conn = get_connection()
    reservations = conn.get_all_instances(filters={"tag:env" : "homolog"})
    instances = [i.id for r in reservations for i in r.instances]
    conn.stop_instances(instance_ids=instances)
    return instances

def stop_endofday_instances():
    conn = get_connection()
    reservations = conn.get_all_instances(filters={"tag:stop" : "endofday"})
    instances = [i.id for r in reservations for i in r.instances]
    conn.stop_instances(instance_ids=instances)
    return instances

def terminate_branch_instances():
    conn = get_connection()
    reservations = conn.get_all_instances(filters={"tag:ENV" : "TEST-BRANCH"})
    instances = [i.id for r in reservations for i in r.instances]
    print 'Instances to terminate: '+str(instances)
    conn.terminate_instances(instance_ids=instances)
    print 'Hasta la vista'
    return instances

def start_test_instances():
    conn = get_connection()
    reservations = conn.get_all_instances(filters={"tag:env" : "homolog", "instance-state-name" : "stopped"})
    instances = [i.id for r in reservations for i in r.instances]
    conn.start_instances(instance_ids=instances)
    return instances

def start_windows1_instance():
    conn = get_connection()
    reservations = conn.get_all_instances(filters={"tag:Name":"srvp-windows-1", "instance-state-name" : "stopped"})
    instances = [i.id for r in reservations for i in r.instances]
    conn.start_instances(instance_ids=instances)
    return instances

def start_windows2_instance():
    conn = get_connection()
    reservations = conn.get_all_instances(filters={"tag:Name":"srvp-windows-2", "instance-state-name" : "stopped"})
    instances = [i.id for r in reservations for i in r.instances]
    conn.start_instances(instance_ids=instances)
    return instances

def stop_windows1_instance():
    conn = get_connection()
    reservations = conn.get_all_instances(filters={"tag:Name":"srvp-windows-1"})
    instances = [i.id for r in reservations for i in r.instances]
    conn.stop_instances(instance_ids=instances)
    return instances

def stop_windows2_instance():
    conn = get_connection()
    reservations = conn.get_all_instances(filters={"tag:Name":"srvp-windows-2"})
    instances = [i.id for r in reservations for i in r.instances]
    conn.stop_instances(instance_ids=instances)
    return instances



def find_by_name(conn,name):
    reservations = conn.get_all_instances(filters={"tag:NAME" : name})
    instances = [i for r in reservations for i in r.instances]
    if len(instances) > 0:
        return instances[0]
    return None

def branch_test_instance(branch_name='branch'):
    import time
    ec2_name = 'test-'+branch_name
    conn = boto.ec2.connect_to_region("sa-east-1",aws_access_key_id=AWS_KEY,aws_secret_access_key=AWS_SECRET)
    instance = find_by_name(conn,ec2_name)
    if instance == None:
        reservation = conn.run_instances('ami-dd991bb1',key_name='GuicheTest',instance_type='t3.medium',security_groups=['ssh-connections','http-group'])
        instance = reservation.instances[0]
        print 'criando nova instancia '+ec2_name
    elif instance.update() != 'running':
        conn.start_instances(instance_ids=[instance.id])
        print 'iniciando instancia %s que ja existia'%(ec2_name,)
    else:
        print 'instancia %s ja esta rodando'%(ec2_name,)
    status = instance.update()
    while status == 'pending':
        print 'status pendente'
        time.sleep(10)
        status = instance.update()
    print 'Iniciada instancia'
    instance.add_tag('ENV', 'TEST-BRANCH')
    instance.add_tag('branch', branch_name)
    instance.add_tag('Name', ec2_name)
    return [instance]


#branch_test_instance()
#terminate_branch_instances()
